let carousel = document.querySelector('.container');
let circles = document.querySelectorAll('.circle');
let rotation = 0;
let circle = 2;


document.querySelector(`.n${circle}`).classList.add('active');
document.querySelector(`.n${circle - 1}`).classList.add('active');
document.querySelector(`.n${circle + 1}`).classList.add('active');
document.querySelector(`.n${circle - 1}`).classList.add('small');
document.querySelector(`.n${circle + 1}`).classList.add('small');


// ---------------------------------------------------------------------

function turnRight() {
      if (circle == 6) {
            document.querySelector(`.n1`).style.transition = '0s';
            document.querySelector(`.n1`).classList.remove('active');
            document.querySelector(`.n1`).classList.remove('small');
      } else {
            document.querySelector(`.n${circle + 1}`).style.transition = '0s';
            document.querySelector(`.n${circle + 1}`).classList.remove('active');
            document.querySelector(`.n${circle + 1}`).classList.remove('small');
      }
      document.querySelector(`.n${circle}`).classList.add('small');
      document.querySelector(`.n${circle}`).classList.remove('big');

      if (circle == 1) {
            circle = 6;
      } else {
            circle--;
      }

      document.querySelector(`.n${circle}`).classList.remove('small');
      document.querySelector(`.n${circle}`).classList.remove('middle');
      document.querySelector(`.n${circle}`).classList.add('big'); 
     
      if (circle == 1) {
            document.querySelector(`.n6`).classList.add('active');
            document.querySelector(`.n6`).classList.add('middle');
      } else {
            document.querySelector(`.n${circle - 1}`).classList.add('active');
            document.querySelector(`.n${circle - 1}`).classList.add('middle');
      }

      rotation += 60;
      carousel.style.transform = `rotate(${rotation}deg)`;
}



function turnLeft() {
      if (circle == 1) {
            document.querySelector(`.n6`).style.transition = '0s';
            document.querySelector(`.n6`).classList.remove('active');
            document.querySelector(`.n6`).classList.remove('small');
      } else {
            document.querySelector(`.n${circle - 1}`).style.transition = '0s';
            document.querySelector(`.n${circle - 1}`).classList.remove('active');
            document.querySelector(`.n${circle - 1}`).classList.remove('small');
      }
      document.querySelector(`.n${circle}`).classList.add('small');
      document.querySelector(`.n${circle}`).classList.remove('big');

      if (circle == 6) {
            circle = 1;
      } else {
            circle++;
      }

      document.querySelector(`.n${circle}`).classList.remove('small');
      document.querySelector(`.n${circle}`).classList.remove('middle');
      document.querySelector(`.n${circle}`).classList.add('big'); 
      

      if (circle == 6) {
            document.querySelector(`.n1`).classList.add('active');
            document.querySelector(`.n1`).classList.add('middle');
      } else {
            document.querySelector(`.n${circle + 1}`).classList.add('active');
            document.querySelector(`.n${circle + 1}`).classList.add('middle');
      }

      rotation -= 60;
      carousel.style.transform = `rotate(${rotation}deg)`;
}
